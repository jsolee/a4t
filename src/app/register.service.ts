import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Account } from './registration/account';

@Injectable()
export class RegisterService {

  constructor(private http: Http) { }

  ngOnInit() {

  }

  postDetails(account) {
    console.log(account)
    return this.http.post("http://ys-training.gq/api/Account/Register", account)
      .map(res => res.json());
  }
}
