import { Component, OnInit, Input } from '@angular/core';
import { RegisterService } from '../register.service';
import { Account } from './account';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  account: Account;

  constructor(private registerService: RegisterService)
  {
    this.account = new Account();
  }

  ngOnInit() {
    this.formData = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", Validators.required),
      confirmPassword: new FormControl("", Validators.required),
      fName: new FormControl("", Validators.required),
      mName: new FormControl("", Validators.required),
      lName: new FormControl("", Validators.required),
      gender: new FormControl("", Validators.required)
    });
  }

  onSubmit(data) {
    if (data.password != data.confirmPassword) {
      alert("Passwords does not match!");
    }
      else {
        this.account = data;
      this.registerService.postDetails(this.account).subscribe(response => { console.log(response) })
      alert("Registration successful!");
      }
    //console.log(this.account);
  }
}
