﻿export class Account {
  email: string;
  password: string;
  confirmPassword: string;
  fName: string;
  mName: string;
  lName: string;
  gender: string;
}